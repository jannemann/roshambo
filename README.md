# ROSHAMBO! Rock-paper-scissors in HipChat

This was my Gradlassian bootcamp project that I did in the first few weeks at Atlassian. This Connect add-on is the first one I've have ever made so there are some bad practices that needs to be cleaned up.

You can install it to your HipChat rooms from the Marketplace: https://marketplace.atlassian.com/plugins/roshambo/cloud/overview



## Development Setup

Create a database named `rps` in your PostgreSQL databse.

```
git clone https://bitbucket.org/khanhfucius/roshambo.git
cd roshambo
npm install
```

Turn on ngrok.

```
ngrok 3000
```
Set your `localBaseUrl` in `config.json` to your ngrok url.

Start the app.
```
npm start
```

## Media
* http://mashable.com/2016/02/18/atlassian-game-hipchat/#K._K3Fpz0Eq8
* http://www.heise.de/tp/news/Schere-Stein-Papier-3113437.html