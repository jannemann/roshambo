var http = require('request');
var cors = require('cors');
var uuid = require('uuid');
var _ = require('underscore');
var RSVP = require('rsvp');

// This is the heart of your HipChat Connect add-on. For more information,
// take a look at https://developer.atlassian.com/hipchat/guide
module.exports = function (app, addon) {

    var hipchat = require('../lib/hipchat')(addon);
    var playerRecordController = require('../lib/player-record-controller')(addon);
    var gameHelper = require('../lib/game-helper')(addon, hipchat);
    var messenger = require('../lib/messenger')(hipchat);

    // simple healthcheck
    app.get('/healthcheck', function (req, res) {
        res.send('OK');
    });

    // Root route. This route will serve the `addon.json` unless a homepage URL is
    // specified in `addon.json`.
    app.get('/',
        function (req, res) {
            // Use content-type negotiation to choose the best way to respond
            res.format({
                // If the request content-type is text-html, it will decide which to serve up
                'text/html': function () {
                    res.redirect(addon.descriptor.links.homepage);
                },
                // This logic is here to make sure that the `addon.json` is always
                // served up when requested by the host
                'application/json': function () {
                    res.redirect('/atlassian-connect.json');
                }
            });
        }
    );

    // This is an example route that's used by the default for the configuration page
    // https://developer.atlassian.com/hipchat/guide/hipchat-ui-extensions/configuration-page
    app.get('/config',
        // Authenticates the request using the JWT token in the request
        addon.authenticate(),
        function (req, res) {
            // The `addon.authenticate()` middleware populates the following:
            // * req.clientInfo: useful information about the add-on client such as the
            //   clientKey, oauth info, and HipChat account info
            // * req.context: contains the context data accompanying the request like
            //   the roomId
            res.render('config', req.context);
        }
    );

    // This is an example glance that shows in the sidebar
    // https://developer.atlassian.com/hipchat/guide/hipchat-ui-extensions/glances
    app.get('/glance',
        cors(),
        addon.authenticate(),
        function (req, res) {

            playerRecordController.getPlayerRecord(req.identity.userId, req.clientInfo.clientKey).then(function (data) {

                var pendingGamesCount = 0;

                if (data) {
                    var pRecord = data;
                    Object.keys(pRecord.games).forEach(function (gameKey) {
                        if (pRecord.games[gameKey].myMove === null) {
                            pendingGamesCount++;
                        }
                    })
                }

                res.json({
                    "label": {
                        "type": "html",
                        "value": pendingGamesCount > 0 ? ("<strong>" + pendingGamesCount + "</strong> pending game(s)") : "ROSHAMBO!"
                    }
                });
            }, function (error) {
                console.log('failed', error);
            });


        }
    );


    app.get('/sidebar',
        addon.authenticate(),
        function (req, res) {
            playerRecordController.getPlayerRecord(req.identity.userId, req.clientInfo.clientKey).then(function (data) {

                var pRecord;

                var resJson = {
                    identity: req.identity,
                    isDarkTheme: req.query.theme === 'dark',
                    theme: req.query.theme,
                    hasPending: false
                }

                if (data && data !== undefined) {
                    pRecord = data;


                    var oldGames = Object.keys(pRecord.oldGames).map(function(key, index) {
                        var g = pRecord.oldGames[key]
                        g.gameKey = key;
                        if (g.result === 'win') {
                            g.oppositeOfResult= 'lose';
                            g.lozClass = 'aui-lozenge-success';
                        } else if (g.result === 'lose') {
                            g.oppositeOfResult = 'win';
                            g.lozClass = 'aui-lozenge-error';
                        } else {
                            g.oppositeOfResult = 'draw';
                            g.lozClass = '';
                        }
                        return g;
                    }).reverse();


                    resJson = {
                        identity: req.identity,
                        isDarkTheme: req.query.theme === 'dark',
                        theme: req.query.theme,
                        record: pRecord,
                        oldGames: oldGames,
                        hasPending: Object.keys(pRecord.games).length > 0
                    };
                }

                res.render('sidebar', resJson);
            }, function (error) {
                console.log('failed', error);
            });
        }
    );

    app.get('/update_sidebar',
        addon.authenticate(),
        function (req, res) {
            playerRecordController.getPlayerRecord(req.identity.userId, req.clientInfo.clientKey).then(function (data) {
                var pRecord;

                if (data) {
                    pRecord = data;

                    var oldGames = Object.keys(pRecord.oldGames).map(function(key, index) {
                        var g = pRecord.oldGames[key]
                        g.gameKey = key;
                        if (g.result === 'win') {
                            g.oppositeOfResult= 'lose';
                            g.lozClass = 'aui-lozenge-success';
                        } else if (g.result === 'lose') {
                            g.oppositeOfResult = 'win';
                            g.lozClass = 'aui-lozenge-error';
                        } else {
                            g.oppositeOfResult = 'draw';
                            g.lozClass = '';
                        }
                        return g;
                    }).reverse();

                    res.json({
                        pendingGames: pRecord.games,
                        oldGames: oldGames,
                        wins: pRecord.wins,
                        losts: pRecord.losts,
                        draws: pRecord.draws
                    });
                } else {
                    res.sendStatus(400);
                }

            }, function (error) {
                console.log('failed', error);
            });
        }
    );


    app.post('/challenge',
        addon.authenticate(),
        function (req, res) {
            res.sendStatus(200);
            var sender = req.context.item.message.from;
            var mentions = req.context.item.message.mentions;
            var clientInfo = req.clientInfo;
            var roomId = req.context.item.room.id;
            var cmd = gameHelper.commandParser(req.body.item.message.message);
            var isRpsls = cmd.command === '/rpsls';

            if (mentions.length === 0) {

                if (cmd.args.length) {
                    switch (cmd.args[0]) {
                        case ':top':
                            playerRecordController.getAll(req.clientInfo.clientKey).then(function (data) {
                                    var playerRecords = data.filter(function (el) {
                                        return el.games instanceof Object && el.oldGames instanceof Object;
                                    });

                                    playerRecords.map(function (pR) {
                                        pR.oldGames = _.values(pR.oldGames);
                                        if (pR.oldGames.length >= 10) {
                                            pR.winrate = Math.round(pR.wins / (pR.wins + pR.losts) * 100);
                                        } else {
                                            pR.winrate = 0;
                                        }
                                        return pR;
                                    });

                                    var topTen = _.first(_.sortBy(playerRecords, 'winrate').reverse(), 10);

                                    // send message displaying the results
                                    messenger.sendTopTen(clientInfo, roomId, topTen);

                                },
                                function (err) {
                                    console.log(err)
                                });
                            break;
                        case ':bottom':
                            playerRecordController.getAll(req.clientInfo.clientKey).then(function (data) {
                                    var playerRecords = data.filter(function (el) {
                                        return el.games instanceof Object && el.oldGames instanceof Object;
                                    });

                                    playerRecords.map(function (pR) {
                                        pR.oldGames = _.values(pR.oldGames);
                                        if (pR.oldGames.length >= 10) {
                                            pR.loserate = Math.round(pR.losts / (pR.wins + pR.losts) * 100);
                                        } else {
                                            pR.loserate = 0;
                                        }
                                        return pR;
                                    });

                                    var bottomTen = _.first(_.sortBy(playerRecords, 'loserate').reverse(), 10);


                                    // send message displaying the results
                                    messenger.sendBottomTen(clientInfo, roomId, bottomTen);

                                },
                                function (err) {
                                    console.log(err)
                                });
                            break;
                        case 'help':
                            messenger.sendHelp(clientInfo, roomId);
                            break;
                        default:
                            messenger.sendHelp(clientInfo, roomId);
                    }

                } else {
                    messenger.sendHelp(clientInfo, roomId);
                }

            } else if (mentions.length > 1) {
                messenger.sendTooManyMentions(clientInfo, roomId);
            } else {
                var player1 = sender;
                var player2 = mentions[0];

                if (cmd.args[0] == ':stats') {

                    var playerRecordPromises = [playerRecordController.getPlayerRecord(player1.id, clientInfo.clientKey),
                                                playerRecordController.getPlayerRecord(player2.id, clientInfo.clientKey)];

                    RSVP.all(playerRecordPromises).then(function(playerRecords) {
                        var playerRecord1 = playerRecords[0];
                        var playerRecord2 = playerRecords[1];

                        if (playerRecord2 === null) {
                            messenger.sendNoStats(clientInfo, roomId, player2.mention_name);
                        } else {
                            playerRecord2 = gameHelper.getStatsFor(playerRecord2, isRpsls);
                            messenger.sendStatsOf(clientInfo, roomId, playerRecord2, isRpsls, addon.descriptor.links.baseUrl);
                        }
                    }).catch(function(err) {
                        console.log(err);
                    })


                } else {

                    if (player1.id === player2.id) {
                        messenger.sendSelfInsult(clientInfo, roomId);
                    } else {

                        var numGames = 1;

                        if (cmd.args.length > 1) {
                            console.log('Awww snap. This guy wants to play more than once!');
                            numGames = parseFloat(cmd.args[1]);
                        }

                        if (isNaN(numGames) || numGames > 10 || numGames === 0 || numGames < 1 || gameHelper.isFloat(numGames)) {
                            console.log('Invalid number of games.');
                            hipchat.sendMessage(clientInfo, roomId, "(no) Please enter a number between 1 and 10.", {options: {format: 'text'}});
                            numGames = 0;
                        }

                        if (numGames) {
                            playerRecordController.getPlayerRecord(player1.id, clientInfo.clientKey).then(
                                function (result) {

                                    var p1Record;
                                    if (result) {
                                        p1Record = result;
                                    } else {
                                        console.log("Creating new user record for", player1.name);
                                        p1Record = playerRecordController.createPlayerRecord(player1.name, player1.mention_name);
                                    }

                                    playerRecordController.getPlayerRecord(player2.id, clientInfo.clientKey).then(
                                        function (result) {

                                            var p2Record;
                                            if (result) {
                                                p2Record = result;

                                            } else {
                                                console.log("Creating new user record for", player2.name);
                                                p2Record = playerRecordController.createPlayerRecord(player2.name, player2.mention_name);
                                            }

                                            var gameId = new Date().getTime();

                                            for (var i = 0; i < numGames; i++) {

                                                var thisGameId = gameId + gameHelper.getSuffixIndexFor(i);

                                                p1Record.games[thisGameId] = {
                                                    isRpsls: isRpsls,
                                                    groupId: gameId,
                                                    round: i,
                                                    bestOf: numGames,
                                                    otherPlayer: {
                                                        id: player2.id,
                                                        name: player2.name,
                                                        mention: player2.mention_name
                                                    },
                                                    myMove: null,
                                                    theirMove: null,
                                                    result: null
                                                };

                                                p2Record.games[thisGameId] = {
                                                    isRpsls: isRpsls,
                                                    groupId: gameId,
                                                    round: i,
                                                    bestOf: numGames,
                                                    otherPlayer: {
                                                        id: player1.id,
                                                        name: player1.name,
                                                        mention: player1.mention_name
                                                    },
                                                    myMove: null,
                                                    theirMove: null,
                                                    result: null
                                                };
                                            }

                                            playerRecordController.updatePlayerRecord(player1.id, p1Record, req);
                                            playerRecordController.updatePlayerRecord(player2.id, p2Record, req);

                                            gameHelper.updateGlance(roomId, player1.id, p1Record, req);
                                            gameHelper.updateGlance(roomId, player2.id, p2Record, req);

                                        }, function (error) {
                                            console.log('failed', error);
                                        }
                                    )

                                },
                                function (error) {
                                    console.log('failed', error);
                                }
                            );

                            messenger.sendChallengeNotification(clientInfo, roomId, player1, player2, numGames, isRpsls)
                                .then(function (data) {
                                    //res.sendStatus(200);
                                });
                        }


                    }
                }


            }
        }
    );


    app.post('/play',
        addon.authenticate(),
        function (req, res) {
            try {
                var params = req.body;
                var playerId = params.playerId;
                var gameKey = params.gameKey;
                var action = params.action;
                var otherPlayerId = params.otherPlayerId;

                var playerRecordPromises = [playerRecordController.getPlayerRecord(playerId, req.clientInfo.clientKey),
                    playerRecordController.getPlayerRecord(otherPlayerId, req.clientInfo.clientKey)];

                RSVP.all(playerRecordPromises).then(function (playerRecords) {
                    var playerRecord = playerRecords[0];
                    var player2Record = playerRecords[1];

                    playerRecord.games[gameKey].myMove = action;

                    if (player2Record.games[gameKey] === undefined || player2Record.games[gameKey] === null) {
                        // TODO: figure out this bug... for now just delete the game.
                        console.log("Game is malformed. Deleting it...");
                        delete playerRecord.games[gameKey];
                        return;
                    }

                    if (player2Record.games[gameKey].myMove) {
                        var action2 = player2Record.games[gameKey].myMove;

                        var result;
                        if (playerRecord.games[gameKey].isRpsls) {
                            result = gameHelper.rockPaperScissorsLizardSpock(action, action2);
                        } else {
                            result = gameHelper.rockPaperScissors(action, action2);
                        }

                        // update both player records based on the results of the game played
                        if (result === 'draw') {
                            playerRecord.games[gameKey].result = 'draw';
                            player2Record.games[gameKey].result = 'draw';
                            playerRecord.draws++;
                            player2Record.draws++;
                        } else if (result === 'win') {
                            playerRecord.games[gameKey].result = 'win';
                            player2Record.games[gameKey].result = 'lose';
                            playerRecord.wins++;
                            player2Record.losts++;
                        } else {
                            playerRecord.games[gameKey].result = 'lose';
                            player2Record.games[gameKey].result = 'win';
                            playerRecord.losts++;
                            player2Record.wins++;
                        }

                        playerRecord.oldGames[gameKey] = playerRecord.games[gameKey];
                        playerRecord.oldGames[gameKey].theirMove = player2Record.games[gameKey].myMove;

                        player2Record.oldGames[gameKey] = player2Record.games[gameKey];
                        player2Record.oldGames[gameKey].theirMove = playerRecord.games[gameKey].myMove;

                        // remove game from pending games
                        delete playerRecord.games[gameKey];
                        delete player2Record.games[gameKey];

                        // update both games objects with score and results
                        playerRecordController.updatePlayerRecord(playerId, playerRecord, req);
                        playerRecordController.updatePlayerRecord(otherPlayerId, player2Record, req);

                        var bestOf = playerRecord.oldGames[gameKey].bestOf;

                        if (bestOf > 1) { // is there more than one game

                            var baseId = playerRecord.oldGames[gameKey].groupId;
                            var games = []

                            // get all the games in this group of games
                            for (var i = 0; i < bestOf; i++) {
                                var thisGameKey = baseId + gameHelper.getSuffixIndexFor(i);
                                if (thisGameKey in playerRecord.oldGames)
                                    games.push(playerRecord.oldGames[thisGameKey]);
                            }

                            if (games.length === bestOf) {
                                // all the games have been played
                                messenger.sendBestOfNResults(req.clientInfo, req.context.room_id, addon.descriptor.links.baseUrl, games,
                                    {name: player2Record.oldGames[gameKey].otherPlayer.name, move: action},
                                    {
                                        name: playerRecord.oldGames[gameKey].otherPlayer.name,
                                        move: action2,
                                    });
                            }
                        } else {
                            messenger.sendGameResult(req.clientInfo, req.context.room_id, addon.descriptor.links.baseUrl,
                                {name: player2Record.oldGames[gameKey].otherPlayer.name, move: action, result: result},
                                {
                                    name: playerRecord.oldGames[gameKey].otherPlayer.name,
                                    move: action2,
                                    result: player2Record.oldGames[gameKey].result
                                }
                            )
                        }

                        // update the glances for both users
                        gameHelper.updateGlance(req.identity.roomId, playerId, playerRecord, req);
                        gameHelper.updateGlance(req.identity.roomId, otherPlayerId, player2Record, req);
                    } else {
                        console.log('Player 2 action still pending');
                        console.log('updating records!');
                        playerRecordController.updatePlayerRecord(playerId, playerRecord, req);
                        gameHelper.updateGlance(req.identity.roomId, playerId, playerRecord, req);
                    }

                    res.sendStatus(200);


                }).catch(function (err) {
                    // if any of the promises fails.
                    console.log(err);
                    res.sendStatus(200);
                });


            } catch (e) {
                console.log('Invalid params')
                console.log(e);
            }


        }
    );

    app.get('/avatar', function (req, res) {
        res.sendfile('public/img/roshambo.png');
    });

    // Notify the room that the add-on was installed. To learn more about
    // Connect's install flow, check out:
    // https://developer.atlassian.com/hipchat/guide/installation-flow
    addon.on('installed', function (clientKey, clientInfo, req) {
        messenger.sendInstalledNotification(clientInfo, req.body.roomId);
    });

    // Clean up clients when uninstalled
    addon.on('uninstalled', function (id) {
        addon.settings.client.keys(id + ':*', function (err, rep) {
            rep.forEach(function (k) {
                addon.logger.info('Removing key:', k);
                addon.settings.client.del(k);
            });
        });
    });

};
