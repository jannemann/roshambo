module.exports = function (addon) {

    var PlayerRecord = function (name, mention) {
        this.name = name;
        this.mention = mention;
        this.games = {};
        this.oldGames = {};
        this.wins = 0;
        this.losts = 0;
        this.draws = 0;
    };

    function _getAll(clientKey) {
        return addon.settings._get({clientKey: clientKey});
    }

    function _save(clientKey, userId, playerRecord) {
        addon.settings.set(userId, JSON.stringify(playerRecord), clientKey);
        return playerRecord;
    }

    return {
        createPlayerRecord: function (name, mention) {
            var newPlayerRecord = new PlayerRecord(name, mention);
            return newPlayerRecord;
        },
        getPlayerRecord: function (userId, clientKey) {
            return addon.settings.get(userId, clientKey);
        },
        updatePlayerRecord: function (userId, playerRecord, req) {
            _save(req.clientInfo.clientKey, userId, playerRecord);
            return playerRecord;
        },
        getAll: function (clientKey) {
            return _getAll(clientKey);
        },
        migrateOldGamesToHash: function(userId, playerRecord, req) {
            console.log(playerRecord);
            if (Array.isArray(playerRecord.oldGames)) {
                console.log('Migrating old games of ' + userId);
                var oldGames = playerRecord.oldGames;
                delete playerRecord['oldGames'];
                playerRecord.oldGames = {};
                oldGames.forEach(function(game) {
                    playerRecord.oldGames[game.gameKey] = game;
                });
                _save(req.clientInfo.clientKey, userId, playerRecord);
            } else {
                return playerRecord;
            }
        }
    }
};
