var _ = require('underscore');

module.exports = function (addon, hipchat) {
    var GAME_SUFFICES = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    return {
        rockPaperScissors: function (action1, action2) {
            if (action1 === action2) {
                return 'draw';
            } else if (action1 === 'rock') {
                if (action2 === 'scissors') return 'win';
                else return 'lose';
            } else if (action1 === 'paper') {
                if (action2 === 'rock') return 'win';
                else return 'lose';
            } else if (action1 === 'scissors') {
                if (action2 === 'paper') return 'win';
                else return 'lose';
            } else {
                return null;
            }
        },
        rockPaperScissorsLizardSpock: function (action1, action2) {
            if (action1 === action2) {
                return 'draw';
            }

            switch (action1) {
                case 'rock': // beats scissors and lizard
                    if (action2 === 'scissors') return 'win';
                    else if (action2 === 'lizard') return 'win';
                    else return 'lose';
                    break;
                case 'paper': // beats rock and spock
                    if (action2 === 'rock') return 'win';
                    else if (action2 === 'spock') return 'win';
                    else return 'lose';
                    break;
                case 'scissors': // paper and lizard
                    if (action2 === 'paper') return 'win';
                    else if (action2 === 'lizard') return 'win';
                    else return 'lose';
                    break;
                case 'lizard': // paper and spock
                    if (action2 === 'paper') return 'win';
                    else if (action2 === 'spock') return 'win';
                    else return 'lose';
                    break;
                case 'spock': // scissors and rock
                    if (action2 === 'scissors') return 'win';
                    else if (action2 === 'rock') return 'win';
                    else return 'lose';
                    break;
                default:
                    break;
            }
        },
        updateGlance: function (roomId, userId, pRecord, req) {

            var pendingGamesCount = 0;

            Object.keys(pRecord.games).forEach(function (gameKey) {
                if (pRecord.games[gameKey].myMove === null) {
                    pendingGamesCount++;
                }
            })

            var newGlance = {
                "label": {
                    "type": "html",
                    "value": pendingGamesCount > 0 ? ('<strong>' + pendingGamesCount + '</strong> pending game(s)') : "ROSHAMBO!"
                }
            };

            hipchat.updateUserGlance(req.clientInfo, roomId, userId, "roshambo.glance", newGlance).then(function (data) {
                console.log("Successfully updated glance.");
            }, function (error) {
                console.log("Failed to update glance.", error);
            });
        },
        commandParser: function (originalCmd) {
            var cleanCmd = originalCmd.replace(/\s+/g, ' ').trim();
            var args = cleanCmd.split(' ');
            return {command: args[0], args: args.splice(1, 2)};
        },
        getSuffixIndexFor: function (gameNumber) {
            if (gameNumber < GAME_SUFFICES.length)
                return GAME_SUFFICES[gameNumber];
            else
                return '';
        },
        isFloat: function (n) {
            return Number(n) === n && n % 1 !== 0;
        },
        getStatsFor: function (player, isRpsls) {

            player.scissors = 0;
            player.rock = 0;
            player.paper = 0;

            if (isRpsls) {
                player.lizard = 0;
                player.spock = 0;
            }

            player.wins = 0;
            player.losts = 0;
            player.draws = 0;

            var oldGames = _.values(player.oldGames).filter(function(game) {
                return game.isRpsls === isRpsls;
            });

            oldGames.forEach(function (game) {
                switch (game.myMove) {
                    case 'rock':
                        player.rock++;
                        break;
                    case 'scissors':
                        player.scissors++;
                        break;
                    case 'paper':
                        player.paper++;
                        break;
                    case 'lizard':
                        player.lizard++;
                        break;
                    case 'spock':
                        player.spock++;
                        break;
                    default:
                        break;
                }

                switch (game.result) {
                    case 'win':
                        player.wins++;
                        break;
                    case 'lose':
                        player.losts++;
                        break;
                    case 'draw':
                        player.draws++;
                        break;
                    default:
                        break;
                }
            });

            player.statsOldGames = oldGames;

            return player;
        }
    }
};
