var _ = require('underscore');

module.exports = function (hipchat) {

    var CHALLENGE_MESSAGE_SUFFIX = [
        'some good ol rock-paper-scissors!',
        'a game a of scissors-paper-rock!',
        'a game of JANKENPON!',
        'a game of ROSHAMBO!'
    ];

    var VICTORY_VERB = [
        ' has destroyed ',
        ' has demolished ',
        ' has defeated ',
        ' attained a glorious victory over ',
        ' has just humiliated '
    ];

    function generateChallengeMessage(player1, player2, numGames, isRpsls) {
        var randomIndex = Math.floor(Math.random() * CHALLENGE_MESSAGE_SUFFIX.length);
        if (numGames === 1) {
            var gameText = CHALLENGE_MESSAGE_SUFFIX[randomIndex];
            if (isRpsls) {
                gameText = 'a game of rock-paper-scissors-lizard-spock!';
            }
            return player1.name + ' has challenged ' + player2.name + ' to ' + gameText +
                ' <a href="#" data-target="roshambo.sidebar">Play in the sidebar.</a>';
        } else {
            var gameText = ' games of rock-paper-scissors!';
            if (isRpsls) {
                gameText = ' games of rock-paper-scissors-lizard-spock!';
            }
            return player1.name + ' has challenged ' + player2.name + ' to ' + numGames + gameText +
                ' <a href="#" data-target="roshambo.sidebar">Play in the sidebar.</a>';
        }
    }

    function generateRandomVictoryVerb(winner, loser) {
        var randomIndex = Math.floor(Math.random() * VICTORY_VERB.length);
        return winner + VICTORY_VERB[randomIndex] + loser + '! ';
    }

    return {
        sendInstalledNotification: function (clientInfo, roomId) {
            return hipchat.sendMessage(clientInfo, roomId,
                'ROSHAMBO! has been installed in this room! ' +
                'Type /rps @mention to play scissors-paper-rock with someone.'
            );
        },
        sendHelp: function (clientInfo, roomId) {
            return hipchat.sendMessage(clientInfo, roomId,
                '/rps @mention [1,10] to challenge someone in this room to a game of scissors-paper-rock! <br>' +
                '/rps :stats @mention to get the rock-paper-scissors statistics of a certain user <br>' +
                '/rps :top to get the top ten players <br>' +
                '/rps :bottom to get the list of losers <br>' +
                '/rps help for well... help <br>' +
                '/rpsls @mention [1,10] to challenge someone in this room to a game of rock-paper-scissors-lizard-spock! <br>' +
                '/rpsls :stats @mention to get the rock-paper-scissors-lizard-spock statistics of a certain user <br>'
            );
        },
        sendTooManyMentions: function (clientInfo, roomId) {
            return hipchat.sendMessage(clientInfo, roomId,
                'Easy there tiger! One at a time.'
            );
        },
        sendSelfInsult: function (clientInfo, roomId) {
            return hipchat.sendMessage(clientInfo, roomId,
                "Just use your own hands if you want to play with yourself that much..."
            );
        },
        sendChallengeNotification: function (clientInfo, roomId, player1, player2, numGames, isRpsls) {
            return hipchat.sendMessage(clientInfo, roomId, generateChallengeMessage(player1, player2, numGames, isRpsls));
        },
        sendGameResult: function (clientInfo, roomId, baseUrl, player1, player2) {
            // Images for the the player's moves (rock, paper or scissors)
            var img1 = baseUrl + '/img/' + player1.move + '_L_' + player1.result + '.png';
            var img2 = baseUrl + '/img/' + player2.move + '_R_' + player2.result + '.png';

            // Lozenges with players names and result state (win, lose, draw)
            var loz1Class = 'aui-lozenge',
                loz2Class = 'aui-lozenge';

            if (player1.result === 'win') {
                loz1Class += ' aui-lozenge-success';
                loz2Class += ' aui-lozenge-error';
            } else if (player2.result === 'win') {
                loz1Class += ' aui-lozenge-error';
                loz2Class += ' aui-lozenge-success';
            }

            var loz1 = '<span class="' + loz1Class + '">' + player1.name + '</span>';
            var loz2 = '<span class="' + loz2Class + '">' + player2.name + '</span>';

            // Generate the html for the message
            var messageHtml = loz1 + ' <img src="' + img1 + '" width="50" height="50" style="padding-left: 15px;"/> <img src="' + img2 + '" width="50" height="50"/>' + loz2 +
                '\n<br>';

            if (player1.result === 'draw') {
                messageHtml += "Looks like it's a draw!";
            } else if (player1.result === 'win') {
                messageHtml += generateRandomVictoryVerb(player1.name, player2.name);
            } else {
                messageHtml += generateRandomVictoryVerb(player2.name, player1.name);
            }

            return hipchat.sendMessage(clientInfo, roomId, messageHtml);
        },
        sendBestOfNResults: function (clientInfo, roomId, baseUrl, games, player1, player2) {
            var winCount = 0,
                loseCount = 0,
                drawCount = 0,
                messageHtml = '';

            games.forEach(function(game) {
                if (game.result === 'win') {
                    winCount++;
                } else if (game.result === 'lose') {
                    loseCount++;
                } else {
                    drawCount++;
                }

                var img1 = baseUrl + '/img/' + game.myMove + '_L_' + 'draw' + '.png';
                var img2 = baseUrl + '/img/' + game.theirMove + '_R_' + 'draw' + '.png';

                var loz1Class = 'aui-lozenge',
                    loz2Class = 'aui-lozenge';

                if (game.result === 'win') {
                    img1 = baseUrl + '/img/' + game.myMove + '_L_' + 'win' + '.png';
                    img2 = baseUrl + '/img/' + game.theirMove + '_R_' + 'lose' + '.png';
                    loz1Class += ' aui-lozenge-success';
                    loz2Class += ' aui-lozenge-error';
                } else if (game.result === 'lose') {
                    img1 = baseUrl + '/img/' + game.myMove + '_L_' + 'lose' + '.png';
                    img2 = baseUrl + '/img/' + game.theirMove + '_R_' + 'win' + '.png';
                    loz1Class += ' aui-lozenge-error';
                    loz2Class += ' aui-lozenge-success';
                }

                var loz1 = '<span class="' + loz1Class + '">' + player1.name + '</span>';
                var loz2 = '<span class="' + loz2Class + '">' + player2.name + '</span>';

                // Generate the html for the message
                var gameHtml = loz1 + ' <img src="' + img1 + '" width="30" height="30" style="padding-left: 15px;"/> <img src="' + img2 + '" width="30" height="30"/>' + loz2;

                messageHtml += gameHtml + '\n<br>';

            });

            // determine the final result

            if ((winCount === 0 && loseCount === 0) || winCount === loseCount) {
                messageHtml += "Looks like it's a draw!";
            } else if (winCount > loseCount) {
                messageHtml += generateRandomVictoryVerb(player1.name, player2.name) + ' [' + winCount + '-' + loseCount + ']';
            } else {
                messageHtml += generateRandomVictoryVerb(player2.name, player1.name) + ' [' + loseCount + '-' + winCount + ']';
            }


            return hipchat.sendMessage(clientInfo, roomId, messageHtml);
        },
        sendTopTen: function (clientInfo, roomId, topTen) {
            var message = 'Top ten winners (with more than 10 games): <br>';
            topTen.forEach(function (player) {
                if (player.oldGames.length >= 10) {
                    message += '<span class="aui-lozenge aui-lozenge-success">' + player.winrate + '%</span> <span class="aui-lozenge">' + player.oldGames.length + ' games</span> &nbsp;&nbsp; @' + player.mention + '\n<br>';
                }
            });
            return hipchat.sendMessage(clientInfo, roomId, message);
        },
        sendBottomTen: function (clientInfo, roomId, bottomTen) {
            var message = 'Top ten losers (with more than 10 games): <br>';
            bottomTen.forEach(function (player) {
                if (player.oldGames.length >= 10) {
                    message += '<span class="aui-lozenge aui-lozenge-error">' + player.loserate + '%</span> <span class="aui-lozenge">' + player.oldGames.length + ' games</span> &nbsp;&nbsp; @' + player.mention + '\n<br>';
                }
            });
            return hipchat.sendMessage(clientInfo, roomId, message);
        },
        sendStatsOf: function (clientInfo, roomId, playerRecord, isRpsls, baseUrl) {

            var oldGames = playerRecord.statsOldGames;

            var winRate = oldGames.length === 0 ? 0 : Math.round(100 * playerRecord.wins / oldGames.length);
            var loseRate = oldGames.length === 0 ? 0 : Math.round(100 * playerRecord.losts / oldGames.length);
            var drawRate = oldGames.length === 0 ? 0 : Math.round(100 * playerRecord.draws / oldGames.length);

            var rockRate = oldGames.length === 0 ? 0 : Math.round(100 * playerRecord.rock / oldGames.length);
            var paperRate = oldGames.length === 0 ? 0 : Math.round(100 * playerRecord.paper / oldGames.length);
            var scissorsRate = oldGames.length === 0 ? 0 : Math.round(100 * playerRecord.scissors / oldGames.length);


            // all the html things
            var rockHtml = '<span class="aui-lozenge">' + playerRecord.rock + ' <img src="' + baseUrl + '/img/rock.svg" height="15" /> (' + rockRate + '%)</span> ';
            var paperHtml = '<span class="aui-lozenge">' + playerRecord.paper + ' <img src="' + baseUrl + '/img/paper.svg" height="15" /> (' + paperRate + '%)</span> ';
            var scissorsHtml = '<span class="aui-lozenge">' + playerRecord.scissors + ' <img src="' + baseUrl + '/img/scissors.svg" height="15" /> (' + scissorsRate + '%)</span> ';

            var gamesPlayedHtml = '<span class="aui-lozenge aui-lozenge-complete">' + oldGames.length + ' games played</span> ';
            var winRateHtml = '<span class="aui-lozenge aui-lozenge-success">' + playerRecord.wins + ' wins (' + winRate + '%)</span> ';
            var loseRateHtml = '<span class="aui-lozenge aui-lozenge-error">' + playerRecord.losts + ' losses (' + loseRate+ '%)</span> ';
            var drawRateHtml = '<span class="aui-lozenge">' + playerRecord.draws + ' draws (' + drawRate + '%)</span> ';

            var message = playerRecord.name + '\'s (@' + playerRecord.mention + ')' + ' statistics: ' + '\n<br>' +
                gamesPlayedHtml + '\n<br>' +
                winRateHtml + loseRateHtml + drawRateHtml + '\n<br>' +
                rockHtml + paperHtml + scissorsHtml;

            if (isRpsls) {
                var lizardRate = oldGames.length === 0 ? 0 : Math.round(100 * playerRecord.lizard / oldGames.length);
                var spockRate = oldGames.length === 0 ? 0 : Math.round(100 * playerRecord.spock / oldGames.length);
                var lizardHtml = '<span class="aui-lozenge">' + playerRecord.lizard + ' <img src="' + baseUrl + '/img/lizard.png" height="15" /> (' + lizardRate + '%)</span> ';
                var spockHtml = '<span class="aui-lozenge">' + playerRecord.spock + ' <img src="' + baseUrl + '/img/spock.png" height="15" /> (' + spockRate + '%)</span> ';
                message += lizardHtml + spockHtml;
            }

            return hipchat.sendMessage(clientInfo, roomId, message);
        },
        sendNoStats: function (clientInfo, roomId, mention) {
            var message = '@' + mention + ' has not played any games yet. You can challenge them to a game with /rps @' + mention;
            return hipchat.sendMessage(clientInfo, roomId, message);
        }
    }
};
